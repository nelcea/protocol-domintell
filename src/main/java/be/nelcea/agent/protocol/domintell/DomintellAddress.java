package be.nelcea.agent.protocol.domintell;

import org.apache.commons.lang.StringUtils;

import java.util.Objects;

/**
 * @author <a href="mailto:ebariaux@nelcea.be">Eric Bariaux</a>
 */
public class DomintellAddress {

   private int address;
   
   public DomintellAddress(String address) throws InvalidDomintellAddressException {
      if (address.startsWith("0x")) {
         if (address.length() > 2) {
            this.address = Integer.parseInt(address.substring(2).trim(), 16);
         } else {
            throw new InvalidDomintellAddressException("Could not parse Domintell address", address);
         }
      } else {
         this.address = Integer.parseInt(address);
      }
   }
   
   public DomintellAddress(int address) {
      this.address = address;
   }
   
   public String toString() {
      return StringUtils.right("      " + Integer.toString(address, 16).toUpperCase(), 6);
   }

   @Override
   public boolean equals(Object o) {
      if (this == o) {
         return true;
      }
      if (o == null || getClass() != o.getClass()) {
         return false;
      }
      DomintellAddress that = (DomintellAddress) o;
      return address == that.address;
   }

   @Override
   public int hashCode() {
      return Objects.hash(address);
   }
}
