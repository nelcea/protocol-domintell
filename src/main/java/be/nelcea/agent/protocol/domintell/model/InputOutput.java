package be.nelcea.agent.protocol.domintell.model;

import org.openremote.model.value.Value;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

/**
 * Java class description...
 * <p>
 * Date : 2019-03-03
 *
 * @author ebariaux
 */
public class InputOutput {

    private int index;
    private String name;
    protected List<Consumer<Value>> consumers;

    public InputOutput(int index, String name) {
        this.index = index;
        this.name = name;
        consumers = new ArrayList<>();
    }

    public void addConsumer(Consumer<Value> consumer) {
        consumers.add(consumer);
        System.out.println("Added consumer " + consumer + " on output " + this);
    }
}
