package be.nelcea.agent.protocol.domintell.model;

import be.nelcea.agent.protocol.domintell.DomintellAddress;
import be.nelcea.agent.protocol.domintell.ModuleType;
import be.nelcea.agent.protocol.domintell.packets.ModulePacket;
import org.openremote.model.value.Value;

import java.util.function.Consumer;

/**
 * Java class description...
 * <p>
 * Date : 2019-03-03
 *
 * @author ebariaux
 */
public abstract class Module {
    private ModuleType type;
    private DomintellAddress address;

    public Module(ModuleType type, DomintellAddress address) {
        this.type = type;
        this.address = address;
    }

    public ModuleType getType() {
        return type;
    }

    public DomintellAddress getAddress() {
        return address;
    }

    public abstract void parseConfiguration(String message);

    public abstract void addConsumer(int ioIndex, Consumer<Value> consumer);

    public abstract void processPayload(String payload);

    public abstract ModulePacket messageForUpdateValue(int ioIndex, Value value);

}
