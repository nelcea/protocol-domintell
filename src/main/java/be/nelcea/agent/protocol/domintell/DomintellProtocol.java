package be.nelcea.agent.protocol.domintell;

import static org.openremote.model.Constants.PROTOCOL_NAMESPACE;

import org.openremote.agent.protocol.AbstractProtocol;
import org.openremote.agent.protocol.MessageProcessor;
import be.nelcea.agent.protocol.domintell.packets.DomintellPacket;
import org.openremote.model.AbstractValueHolder;
import org.openremote.model.asset.AssetAttribute;
import org.openremote.model.asset.agent.ConnectionStatus;
import org.openremote.model.attribute.AttributeEvent;
import org.openremote.model.attribute.AttributeRef;
import org.openremote.model.attribute.AttributeState;
import org.openremote.model.util.Pair;
import org.openremote.model.value.Value;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Consumer;
import java.util.logging.Logger;

public class DomintellProtocol extends AbstractProtocol {

    private static final Logger LOG = Logger.getLogger(DomintellProtocol.class.getName());

    private final static int DEFAULT_PORT = 17481;

    public static final String PROTOCOL_NAME = PROTOCOL_NAMESPACE + ":domintell";
    private static final String PROTOCOL_DISPLAY_NAME = "Domintell";
    private static final String VERSION = "1.0";

    public static final String URL = PROTOCOL_NAME + ":URL";

    public static final String MODULE_TYPE = PROTOCOL_NAME + ":moduleType";
    public static final String MODULE_ADDRESS = PROTOCOL_NAME + ":moduleAddress";
    public static final String IO_INDEX = PROTOCOL_NAME + ":ioIndex";


    final protected Map<AttributeRef, Consumer<ConnectionStatus>> statusConsumerMap = new ConcurrentHashMap<>();
    final protected Map<String, DomintellGateway> gateways = new HashMap<>();

    @Override
    public String getProtocolName() {
        return PROTOCOL_NAME;
    }

    @Override
    public String getProtocolDisplayName() {
        return PROTOCOL_DISPLAY_NAME;
    }

    @Override
    public String getVersion() {
        return VERSION;
    }

    @Override
    protected void doLinkProtocolConfiguration(AssetAttribute protocolConfiguration) {
        Optional<String> url = protocolConfiguration.getMetaItem(URL).flatMap(AbstractValueHolder::getValueAsString);

        if (!url.isPresent()) {
            LOG.severe("No URI provided for protocol configuration: " + protocolConfiguration);
            updateStatus(protocolConfiguration.getReferenceOrThrow(), ConnectionStatus.ERROR_CONFIGURATION);
            return;
        }

        if (!protocolConfiguration.isEnabled()) {
            LOG.severe("Domintell protocol configuration not enabled");
            updateStatus(protocolConfiguration.getReferenceOrThrow(), ConnectionStatus.DISABLED);
            return;
        }

        final AttributeRef protocolRef = protocolConfiguration.getReferenceOrThrow();

        // Look for existing network
        DomintellGateway gateway = null;
        Pair<DomintellGateway, Consumer<ConnectionStatus>> gatewayConsumerPair = null;

        MessageProcessor<DomintellPacket> messageProcessor = new DomintellMessageProcessor(url.get(), executorService);

        Consumer<ConnectionStatus> statusConsumer = status -> updateStatus(protocolRef, status);

        gateway = gateways.computeIfAbsent(
                url.get(), key ->
                        new DomintellGateway(messageProcessor, executorService)
        );

        gateway.addConnectionStatusConsumer(statusConsumer);
        gateway.connect();

        statusConsumerMap.put(protocolRef, statusConsumer);
    }

    @Override
    protected void doUnlinkProtocolConfiguration(AssetAttribute protocolConfiguration) {
        DomintellGateway gateway = getDomintellGateway(protocolConfiguration);
        gateway.disconnect();

        AttributeRef protocolRef = protocolConfiguration.getReferenceOrThrow();
        Consumer<ConnectionStatus> statusConsumer = statusConsumerMap.get(protocolRef);
        gateway.removeConnectionStatusConsumer(statusConsumer);
        statusConsumerMap.remove(protocolRef);
    }

    @Override
    protected void doLinkAttribute(AssetAttribute attribute, AssetAttribute protocolConfiguration) {
        DomintellGateway gateway = getDomintellGateway(protocolConfiguration);

        final AttributeRef attributeRef = attribute.getReferenceOrThrow();

        gateway.addAttribute(attributeRef, getModuleType(attribute, attributeRef), getDomintellAddress(attribute, attributeRef),
                getIOIndex(attribute, attributeRef), value -> handleValueChange(attributeRef, value));
    }

    @Override
    protected void doUnlinkAttribute(AssetAttribute attribute, AssetAttribute protocolConfiguration) {

    }

    @Override
    protected void processLinkedAttributeWrite(AttributeEvent event, AssetAttribute protocolConfiguration) {
        DomintellGateway gateway = getDomintellGateway(protocolConfiguration);

        // Includes attribute ref -> attribute name and value
        // and protocol configuration -> URL of domintell gateway
        // but not attribute configuration -> need to be known in gateway
        // No, can get it back with

        AssetAttribute attribute = getLinkedAttribute(event.getAttributeRef());

// e.g. event value is true and we have a BIR -> turn it on



        try {
            String moduleType = attribute.getMetaItem(MODULE_TYPE)
                    .flatMap(AbstractValueHolder::getValueAsString).get();
            String address = attribute.getMetaItem(MODULE_ADDRESS)
                    .flatMap(AbstractValueHolder::getValueAsString).get();
            Integer ioIndex = attribute.getMetaItem(IO_INDEX)
                    .flatMap(AbstractValueHolder::getValueAsInteger).get();



            gateway.updateValue(ModuleType.valueOf(moduleType), new DomintellAddress(address), ioIndex, event.getValue().get());

//            gateway.sendMessage(moduleType + a.toString() + "-" + Integer.toString(ioIndex) + "%" + (event.getValue().get().toString().equals("true")?"I":"O"));

        } catch (InvalidDomintellAddressException e) {
            e.printStackTrace();
        }

    }

    protected void handleValueChange(AttributeRef attributeRef, Value value) {
        LOG.fine("Domintell protocol received value '" + value + "' for : " + attributeRef);
        updateLinkedAttribute(new AttributeState(attributeRef, value));
    }

    private DomintellGateway getDomintellGateway(AssetAttribute protocolConfiguration) {
        Optional<String> url = protocolConfiguration.getMetaItem(URL).flatMap(AbstractValueHolder::getValueAsString);
        return gateways.get(url.get());
    }

    private ModuleType getModuleType(AssetAttribute attribute, AttributeRef attributeRef) {
        Optional<String> moduleTypeString = attribute.getMetaItem(MODULE_TYPE).flatMap(AbstractValueHolder::getValueAsString);
        if (!moduleTypeString.isPresent()) {
            LOG.severe("No module type for protocol attribute: " + attributeRef);
            return null;
        }

        return ModuleType.valueOf(moduleTypeString.get());
    }

    private DomintellAddress getDomintellAddress(AssetAttribute attribute, AttributeRef attributeRef) {
        Optional<String> domintellAddressString = attribute.getMetaItem(MODULE_ADDRESS).flatMap(AbstractValueHolder::getValueAsString);
        if (!domintellAddressString.isPresent()) {
            return null;
        }
        try {
            return new DomintellAddress(domintellAddressString.get());
        } catch (InvalidDomintellAddressException e) {
            e.printStackTrace();
        }
        return null;
    }

    private Integer getIOIndex(AssetAttribute attribute, AttributeRef attributeRef) {
        Optional<Integer> ioIndexString = attribute.getMetaItem(IO_INDEX).flatMap(AbstractValueHolder::getValueAsInteger);
        if (!ioIndexString.isPresent()) {
            return null;
        }
        return ioIndexString.get();
    }
}
