package be.nelcea.agent.protocol.domintell.model;

import org.openremote.model.value.Values;

/**
 * Java class description...
 * <p>
 * Date : 2019-04-30
 *
 * @author ebariaux
 */
public class LevelOutput extends InputOutput {

    private int level;

    public LevelOutput(int index, String name) {
        super(index, name);
    }

    public void updateState(int level) {
        if (this.level != level) {
            this.level = level;
            consumers.forEach(consumer -> consumer.accept(Values.create(level)));
        }
    }
}
