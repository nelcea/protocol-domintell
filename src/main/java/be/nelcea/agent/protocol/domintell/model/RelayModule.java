package be.nelcea.agent.protocol.domintell.model;

import be.nelcea.agent.protocol.domintell.DomintellAddress;
import be.nelcea.agent.protocol.domintell.packets.ModulePacket;
import be.nelcea.agent.protocol.domintell.ModuleType;
import org.openremote.model.value.BooleanValue;
import org.openremote.model.value.Value;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Consumer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Java class description...
 * <p>
 * Date : 2019-03-03
 *
 * @author ebariaux
 */
public class RelayModule extends Module {

    private Map<Integer, BinaryInputOutput> outputs;

    public RelayModule(ModuleType type, DomintellAddress address) {
        super(type, address);
        outputs = new HashMap<>();
        for (int i = 1; i <= 8; i++) {
            outputs.put(i, new BinaryInputOutput(i, "No name"));
        }
    }

    @Override
    public void parseConfiguration(String message) {
        Pattern pattern = Pattern.compile("-(\\d)(.*)(\\[(.*)\\|(.*)\\|(.*)])");
        // e.g. -1Baie[House|Rez|Salle a manger]
        Matcher match = pattern.matcher(message);
        if (((Matcher) match).matches()) {
            int ioIndex = Integer.parseInt(match.group(1));
            outputs.put(ioIndex, new BinaryInputOutput(ioIndex, match.group(2)));
        }
    }

    @Override
    public void addConsumer(int ioIndex, Consumer<Value> consumer) {
        outputs.get(ioIndex).addConsumer(consumer);
    }

    @Override
    public void processPayload(String payload) {
        System.out.println("Module processing payload " + payload);
        try {
            // O00
            int value = Integer.parseInt(payload.substring(1), 16);
            int bitmask = 1;
            for (int i = 1; i <= 8; i++) {
                outputs.get(i).updateState((value & bitmask) == bitmask);
                bitmask = bitmask<<1;
            }
        } catch (NumberFormatException e) {
            e.printStackTrace();
            // Not understood as an output feedback, do not update ourself
//            log.warn("Invalid feedback received " + payload, e);
        }
    }

    @Override
    public ModulePacket messageForUpdateValue(int ioIndex, Value value) {
        if (value instanceof BooleanValue) {
            if (((BooleanValue) value).getBoolean()) {
                return onPacket(ioIndex);
            } else {
                return offPacket(ioIndex);
            }
        }
        return null;
    }

    private ModulePacket onPacket(int ioIndex) {
        return new ModulePacket(getType(), getAddress(), "-" + Integer.toString(ioIndex) + "%I");
    }

    private ModulePacket offPacket(int ioIndex) {
        return new ModulePacket(getType(), getAddress(), "-" + Integer.toString(ioIndex) + "%O");
    }
}
