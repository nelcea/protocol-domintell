package be.nelcea.agent.protocol.domintell.model;

import be.nelcea.agent.protocol.domintell.ModuleType;

import java.util.HashMap;
import java.util.Map;

/**
 * Java class description...
 * <p>
 * Date : 2019-03-03
 *
 * @author ebariaux
 */
public class ModuleClassRegistry {

    private static Map<ModuleType, Class<? extends Module>> moduleTypeRegistry = new HashMap<>();

    static {
        moduleTypeRegistry.put(ModuleType.BIR, RelayModule.class);
        moduleTypeRegistry.put(ModuleType.DMR, RelayModule.class);

        moduleTypeRegistry.put(ModuleType.DIM, DimmerModule.class);
        moduleTypeRegistry.put(ModuleType.D10, DimmerModule.class);

        moduleTypeRegistry.put(ModuleType.IS4, InputModule.class);
        moduleTypeRegistry.put(ModuleType.IS8, InputModule.class);
    }

    public static Class<? extends Module> getModuleClass(ModuleType type) {
        return moduleTypeRegistry.get(type);
    }

}
