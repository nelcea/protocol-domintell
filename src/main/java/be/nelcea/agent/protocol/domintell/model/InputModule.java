package be.nelcea.agent.protocol.domintell.model;

import be.nelcea.agent.protocol.domintell.DomintellAddress;
import be.nelcea.agent.protocol.domintell.packets.ModulePacket;
import be.nelcea.agent.protocol.domintell.ModuleType;
import org.openremote.model.value.Value;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Consumer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Java class description...
 * <p>
 * Date : 2019-05-01
 *
 * @author ebariaux
 */
public class InputModule extends Module {

    private int numberOfInputs;
    private Map<Integer, BinaryInputOutput> inputs;

    public InputModule(ModuleType type, DomintellAddress address) {
        super(type, address);
        inputs = new HashMap<>();
        numberOfInputs = 0;
        switch(type) {
            case IS4:
                numberOfInputs = 4;
                break;
            case IS8:
                numberOfInputs = 8;
                break;
        }
        for (int i = 1; i <= numberOfInputs; i++) {
            inputs.put(i, new BinaryInputOutput(i, "No name"));
        }
    }

    @Override
    public void parseConfiguration(String message) {
        Pattern pattern = Pattern.compile("-(\\d)(.*)(\\[(.*)\\|(.*)\\|(.*)])");
        // e.g. -1Bureau HG[House|1 étage|Bureau][PUSH=SHORT]
        Matcher match = pattern.matcher(message);
        if (((Matcher) match).matches()) {
            int ioIndex = Integer.parseInt(match.group(1));
            inputs.put(ioIndex, new BinaryInputOutput(ioIndex, match.group(2)));
            //            System.out.println("loc>"+match.group(4)+"-"+match.group(5)+"-"+match.group(6)+"<");
        }
    }

    @Override
    public void addConsumer(int ioIndex, Consumer<Value> consumer) {
        inputs.get(ioIndex).addConsumer(consumer);
    }

    @Override
    public void processPayload(String payload) {
        System.out.println("Module processing payload " + payload);
        try {
            // I00 (even on IS4 2 digits are used to report value, only LSB has information)
            int value = Integer.parseInt(payload.substring(1), 16);
            int bitmask = 1;
            // On IS4, the
            for (int i = 1; i <= numberOfInputs; i++) { // On IS4, this will only process 4 LSB bits
                inputs.get(i).updateState((value & bitmask) == bitmask);
                bitmask = bitmask<<1;
            }
        } catch (NumberFormatException e) {
            e.printStackTrace();
            // Not understood as an output feedback, do not update ourself
            //            log.warn("Invalid feedback received " + payload, e);
        }
    }

    @Override
    public ModulePacket messageForUpdateValue(int ioIndex, Value value) {
        return null;
    }
}
