package be.nelcea.agent.protocol.domintell;

import be.nelcea.agent.protocol.domintell.packets.*;
import org.openremote.agent.protocol.ProtocolExecutorService;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Java class description...
 * <p>
 * Date : 2019-02-20
 *
 * @author ebariaux
 */
public class DomintellMessageProcessor extends AbstractWebSocketMessageProcessor<DomintellPacket> {

    private Pattern infoMessage = Pattern.compile("INFO:(.*):INFO\r\n");
    private Pattern dateTimePattern = Pattern.compile("\\d{1,2}:\\d{1,2} \\d{1,2}/\\d{1,2}/\\d{1,2}\r\n");

    public DomintellMessageProcessor(String uri, ProtocolExecutorService executorService) {
        super(uri, executorService);
    }



    protected DomintellPacket decode(String message) {

        System.out.println("Decode >"+message+"<");

        Matcher infoMatch = infoMessage.matcher(message);
        if (infoMatch.matches()) {
            return new InfoPacket(infoMatch.group(1));
        }
        Matcher dateTimeMatch = dateTimePattern.matcher(message);
        if (dateTimeMatch.matches()) {
            return new DateTimePacket();
        }
        if (message.startsWith("APPINFO")) {
            return new AppInfoPacket(message);
        }

        // TODO : support PONG

        ModulePacket packet = ModulePacket.parseMessage(message);
        return (packet != null)?packet:new MessagePacket(message);
    }

    protected String encode(DomintellPacket packet) {
        return packet.toString();
    }

}
