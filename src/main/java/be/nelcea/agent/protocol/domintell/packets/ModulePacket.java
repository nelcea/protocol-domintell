package be.nelcea.agent.protocol.domintell.packets;

import be.nelcea.agent.protocol.domintell.DomintellAddress;
import be.nelcea.agent.protocol.domintell.InvalidDomintellAddressException;
import be.nelcea.agent.protocol.domintell.ModuleType;

/**
 * Java class description...
 * <p>
 * Date : 2019-03-03
 *
 * @author ebariaux
 */
public class ModulePacket extends DomintellPacket {

    private ModuleType type;
    private DomintellAddress address;
    private String payload;

    public ModulePacket(ModuleType type, DomintellAddress address, String payload) {
        this.type = type;
        this.address = address;
        this.payload = payload;
    }

    /**
     * Tries to create a ModulePacket from the given message and returns null if it can't.
     *
     * @param message
     * @return
     */
    public static ModulePacket parseMessage(String message) {
        // First 3 chars in module type
        // Next 6 is address
        // Then 1 char is "type of data"

        // e.g. IS8  1234I00

        if (message.length() < 9) {
            return null;
        }

        String typeText = message.substring(0, 3);
        ModuleType type;
        try {
            type = ModuleType.valueOf(typeText);
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
            return null;
        }

        String addressText = message.substring(3, 9);
        DomintellAddress address;
        try {
            address = new DomintellAddress("0x" + addressText);
        } catch (InvalidDomintellAddressException e) {
            e.printStackTrace();
            return null;
        }

        return new ModulePacket(type, address, message.substring(9).trim());
    }

    public ModuleType getType() {
        return type;
    }

    public DomintellAddress getAddress() {
        return address;
    }

    public String getPayload() {
        return payload;
    }

    @Override
    public String toString() {
        return type.toString() + address.toString() + payload;
    }
}
