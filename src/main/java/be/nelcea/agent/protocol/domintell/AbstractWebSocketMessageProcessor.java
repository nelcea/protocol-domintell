package be.nelcea.agent.protocol.domintell;

import static org.openremote.model.syslog.SyslogCategory.PROTOCOL;

import io.netty.channel.Channel;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioSocketChannel;
import io.netty.handler.ssl.SslContext;
import io.netty.handler.ssl.SslContextBuilder;
import io.netty.handler.ssl.SslHandler;
import io.netty.handler.ssl.util.InsecureTrustManagerFactory;
import org.openremote.agent.protocol.ProtocolExecutorService;
import org.openremote.model.syslog.SyslogCategory;
import org.openremote.model.util.TextUtil;

import javax.net.ssl.SSLException;
import java.net.InetSocketAddress;
import java.net.SocketAddress;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.logging.Logger;

/**
 * Java class description...
 * <p>
 * Date : 2019-02-25
 *
 * @author ebariaux
 */

public abstract class AbstractWebSocketMessageProcessor<T> extends AbstractNettyMessageProcessor<T> {

        private static final Logger LOG = SyslogCategory.getLogger(PROTOCOL, org.openremote.agent.protocol.AbstractSocketMessageProcessor.class);
        protected String url;

        public AbstractWebSocketMessageProcessor(String url, ProtocolExecutorService executorService) {
            super(executorService);
            TextUtil.requireNonNullAndNonEmpty(url);
            this.url = url;
        }

        @Override
        protected Class<? extends Channel> getChannelClass() {
            return NioSocketChannel.class;
        }

        @Override
        protected SocketAddress getSocketAddress() {
            return new InetSocketAddress(getHost(), getPort());
        }

        @Override
        protected String getSocketAddressString() {
            return getHost() + ":" + getPort();
        }

        @Override
        protected EventLoopGroup getWorkerGroup() {
            return new NioEventLoopGroup(1);
        }

        @Override
        protected URI getURI() {

            // TODO: clean

            URI uri = null;
            try {
                uri = new URI(url);
            } catch (URISyntaxException e) {
                return null;
            }
            return uri;
        }

        /*
        @Override
        protected void configureChannel() {
            super.configureChannel();
            bootstrap.option(ChannelOption.SO_KEEPALIVE, true);
        }
        */

        @Override
    protected SslHandler getSslHandler() {
            SslContext sslCtx = null;
            final boolean ssl = "wss".equalsIgnoreCase(getURI().getScheme());
        if (ssl) {
            try {
                sslCtx = SslContextBuilder.forClient()
                        .trustManager(InsecureTrustManagerFactory.INSTANCE).build();
            } catch (SSLException e) {
                e.printStackTrace();
            }
        } else {
            sslCtx = null;
        }


        return (sslCtx != null)?sslCtx.newHandler(channel.alloc(), getHost(), getPort()):null;
    }




    private int getPort() {
            URI uri = null;
            try {
                uri = new URI(url);
            } catch (URISyntaxException e) {
                return -1;
            }
            String scheme = uri.getScheme() == null? "ws" : uri.getScheme();
            final String host = uri.getHost() == null? "127.0.0.1" : uri.getHost();
            final int port;
            if (uri.getPort() == -1) {
                if ("ws".equalsIgnoreCase(scheme)) {
                    port = 80;
                } else if ("wss".equalsIgnoreCase(scheme)) {
                    port = 443;
                } else {
                    port = -1;
                }
            } else {
                port = uri.getPort();
            }
            return port;
        }

        private String getHost() {
            URI uri = null;
            try {
                uri = new URI(url);
            } catch (URISyntaxException e) {
                return "localhost";
            }
            return uri.getHost() == null? "127.0.0.1" : uri.getHost();
        }


}
