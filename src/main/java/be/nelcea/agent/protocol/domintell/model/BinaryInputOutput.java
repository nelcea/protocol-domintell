package be.nelcea.agent.protocol.domintell.model;

import org.openremote.model.value.Values;

/**
 * Java class description...
 * <p>
 * Date : 2019-03-03
 *
 * @author ebariaux
 */
public class BinaryInputOutput extends InputOutput {

    private boolean state;

    public BinaryInputOutput(int index, String name) {
        super(index, name);
    }

    public void updateState(boolean state) {

        System.out.println("Binary output " + this + ", current state " + this.state + ", update state to " + state);
        if (this.state != state) {
            this.state = state;
            System.out.println("State change, notifying consumers " + consumers);
            consumers.forEach(consumer -> consumer.accept(Values.create(state)));
        }
    }
}
