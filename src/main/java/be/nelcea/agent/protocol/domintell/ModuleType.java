package be.nelcea.agent.protocol.domintell;

/**
 * Java class description...
 * <p>
 * Date : 2019-03-03
 *
 * @author <a href="mailto:ebariaux@nelcea.be">Eric Bariaux</a>
 */
public enum ModuleType {
    BIR,
    DMR,
    TRP,
    DIM,
    D10,
    TSB,
    TE1,
    TE2,
    LC3,
    PBL,
    IS4,
    IS8,
    BU4,
    ET2,
    NT1,
    VAR,
    SYS,
    MEM,
    CLK,
    SFE

}
