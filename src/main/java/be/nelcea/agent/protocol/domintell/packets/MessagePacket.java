package be.nelcea.agent.protocol.domintell.packets;

/**
 * Java class description...
 * <p>
 * Date : 2019-03-03
 *
 * @author ebariaux
 */
public class MessagePacket extends DomintellPacket {

    String message;

    public MessagePacket(String message) {
        this.message = message;
    }

    @Override
    public String toString() {
        return message;
    }

}
