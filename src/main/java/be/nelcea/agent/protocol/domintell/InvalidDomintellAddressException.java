package be.nelcea.agent.protocol.domintell;

/**
 * Exception raised by incorrectly formatted Domintell address when attempting to convert
 * a string representation into an instance of {@link DomintellAddress}
 *
 * @author <a href="mailto:ebariaux@nelcea.be">Eric Bariaux</a>
 */
@SuppressWarnings("serial")
public class InvalidDomintellAddressException extends Exception
{

  // Instance Fields ------------------------------------------------------------------------------

  /**
   * Keep the original invalid string format around, useful for error reporting and logging.
   */
  private String invalidAddress;


  // Constructors ---------------------------------------------------------------------------------

  /**
   * Constructs a new exception instance with a given message and the string representation
   * of a Domintell address that could not be parsed.
   *
   * @param message           exception message
   * @param invalidAddress    original address string that did not convert into
   *                          {@link DomintellAddress} instance
   */
  InvalidDomintellAddressException(String message, String invalidAddress)
  {
    super(message);

    this.invalidAddress = invalidAddress;
  }

  /**
   * Constructs a new exception instance with a given message, string representation of a
   *  Domintell address that could not be parsed and a root exception cause.
   *
   * @param message           exception message
   * @param invalidAddress    original address string that did not convert into
   *                          {@link DomintellAddress} instance
   * @param rootCause         root cause of this exception
   */
  InvalidDomintellAddressException(String message, String invalidAddress, Exception rootCause)
  {
    super(message, rootCause);

    this.invalidAddress = invalidAddress;
  }

  // Instance Methods -----------------------------------------------------------------------------

  /**
   * Returns the string that was attempted to parse into a {@link DomintellAddress} instance.
   *
   * @return  the original Domintell address string that failed to parse correctly.
   */
  String invalidAddress()
  {
    return invalidAddress;
  }
}
