package be.nelcea.agent.protocol.domintell.model;

import be.nelcea.agent.protocol.domintell.DomintellAddress;
import be.nelcea.agent.protocol.domintell.ModuleType;
import be.nelcea.agent.protocol.domintell.packets.ModulePacket;
import org.openremote.model.value.BooleanValue;
import org.openremote.model.value.NumberValue;
import org.openremote.model.value.Value;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Consumer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Java class description...
 * <p>
 * Date : 2019-04-29
 *
 * @author ebariaux
 */
public class DimmerModule extends Module {

    private Map<Integer, LevelOutput> outputs;

    public DimmerModule(ModuleType type, DomintellAddress address) {
        super(type, address);
        outputs = new HashMap<>();
        for (int i = 1; i <= 8; i++) {
            outputs.put(i, new LevelOutput(i, "No name"));
        }
    }

    @Override
    public void parseConfiguration(String message) {
        Pattern pattern = Pattern.compile("-(\\d)(.*)(\\[(.*)\\|(.*)\\|(.*)])");
        // e.g. -1Plafonnier cuisine[House||]
        Matcher match = pattern.matcher(message);
        if (((Matcher) match).matches()) {
            int ioIndex = Integer.parseInt(match.group(1));
            outputs.put(ioIndex, new LevelOutput(ioIndex, match.group(2)));
            //            System.out.println("loc>"+match.group(4)+"-"+match.group(5)+"-"+match.group(6)+"<");
        }
    }

    @Override
    public void addConsumer(int ioIndex, Consumer<Value> consumer) {
        outputs.get(ioIndex).addConsumer(consumer);
    }

    @Override
    public void processPayload(String payload) {
        System.out.println("Module processing payload " + payload);
        try {
            // D 064 0 0 0 0 0 0
            for (int i = 0; i < 8; i++) {
                int level = Integer.parseInt(payload.substring(1 + i * 2, 3 + i * 2).trim(), 16);
                outputs.get(i + 1).updateState(level);
            }
        } catch (NumberFormatException e) {
            e.printStackTrace();
            // Not understood as a scene, do not update ourself
            // log.warn("Invalid feedback received " + payload, e);
        } catch (StringIndexOutOfBoundsException e) {
            e.printStackTrace();
            // Not understood as a scene, do not update ourself
            // log.warn("Invalid feedback received " + payload, e);
        }
    }

    @Override
    public ModulePacket messageForUpdateValue(int ioIndex, Value value) {
        if (value instanceof BooleanValue) {
            if (((BooleanValue) value).getBoolean()) {
                return onPacket(ioIndex);
            } else {
                return offPacket(ioIndex);
            }
        } else if (value instanceof NumberValue) {
            return setLevelPacket(ioIndex, (int)((NumberValue)value).getNumber());
        }
        return null;
    }

    public ModulePacket onPacket(int ioIndex) {
        return new ModulePacket(getType(), getAddress(), "-" + Integer.toString(ioIndex) + "%I");
    }

    public ModulePacket offPacket(int ioIndex) {
        return new ModulePacket(getType(), getAddress(), "-" + Integer.toString(ioIndex) + "%O");
    }

    public ModulePacket setLevelPacket(int ioIndex, int level) {
        return new ModulePacket(getType(), getAddress(),"-" + Integer.toString(ioIndex) + "%D" + Integer.toString(level));
    }

/*    public void queryState(Integer output) {
        gateway.sendCommand(moduleType + address + "-" + Integer.toString(output) + "%S");
    }*/


}
