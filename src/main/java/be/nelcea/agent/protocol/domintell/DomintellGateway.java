package be.nelcea.agent.protocol.domintell;

import be.nelcea.agent.protocol.domintell.model.Configuration;
import be.nelcea.agent.protocol.domintell.model.Module;
import be.nelcea.agent.protocol.domintell.packets.*;
import org.openremote.agent.protocol.MessageProcessor;
import org.openremote.agent.protocol.ProtocolExecutorService;
import org.openremote.model.asset.agent.ConnectionStatus;
import org.openremote.model.attribute.AttributeRef;
import org.openremote.model.value.Value;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;
import java.util.function.Consumer;
import java.util.logging.Logger;

public class DomintellGateway {

    private static final Logger LOG = Logger.getLogger(DomintellProtocol.class.getName());

    protected ConnectionStatus connectionStatus = ConnectionStatus.DISCONNECTED;

    protected MessageProcessor<DomintellPacket> messageProcessor;
    protected ProtocolExecutorService executorService;

    protected final List<Consumer<ConnectionStatus>> connectionStatusConsumers = new ArrayList<>();

    private Configuration configuration;

    private ScheduledFuture pinger;



//    protected final Map<AttributeRef, ScheduledFuture> attributesReads = new ConcurrentHashMap<>();





    public DomintellGateway(MessageProcessor<DomintellPacket> messageProcessor, ProtocolExecutorService executorService) {
        this.messageProcessor = messageProcessor;
        messageProcessor.addConnectionStatusConsumer(this::onConnectionStatusChanged);
        messageProcessor.addMessageConsumer(this::onPacketReceived);
        this.executorService = executorService;
    }

    public synchronized void connect() {
        if (messageProcessor == null) {
            return;
        }

        configuration = new Configuration();

        messageProcessor.connect();
    }

    public void disconnect() {
        pinger.cancel(true);
        pinger = null;

        configuration = null;

        if (messageProcessor != null) {
            messageProcessor.disconnect();
        }
    }

    public ConnectionStatus getConnectionStatus() {
        return messageProcessor.getConnectionStatus();
    }

    public synchronized void addConnectionStatusConsumer(Consumer<ConnectionStatus> connectionStatusConsumer) {
        if (!connectionStatusConsumers.contains(connectionStatusConsumer)) {
            connectionStatusConsumers.add(connectionStatusConsumer);
        }
    }

    public synchronized void removeConnectionStatusConsumer(Consumer<ConnectionStatus> connectionStatusConsumer) {
        connectionStatusConsumers.remove(connectionStatusConsumer);
    }

    protected synchronized void onConnectionStatusChanged(ConnectionStatus connectionStatus) {
        this.connectionStatus = connectionStatus;

        connectionStatusConsumers.forEach(
                consumer -> consumer.accept(connectionStatus)
        );
    }


    protected void onPacketReceived(DomintellPacket packet) {
        System.out.println("Received packet " + packet);
        if (packet instanceof InfoPacket) {
            if (((InfoPacket) packet).getType() == InfoPacket.InfoType.WAITING_LOGIN) {
                messageProcessor.sendMessage(new MessagePacket("LOGINPSW@a:a"));
            } else if (((InfoPacket) packet).getType() == InfoPacket.InfoType.SESSION_OPENED) {
                messageProcessor.sendMessage(new MessagePacket("APPINFO"));

                pinger = executorService.scheduleWithFixedDelay(() -> {
                    messageProcessor.sendMessage(new MessagePacket("HELLO"));
                }, 30, 59, TimeUnit.SECONDS);

            }
        } else if (packet instanceof AppInfoPacket) {
            // Don't process this for now
            // configuration = ((AppInfoPacket) packet).getConfiguration();
        } else if (packet instanceof ModulePacket) {
            // TODO: find out what attribute to update and do

            ModulePacket modulePacket = (ModulePacket)packet;

            System.out.println("Received module packet " + modulePacket);

            Module module = configuration.getModule(modulePacket.getType(), modulePacket.getAddress(), false);

            System.out.println("Configuration " + configuration);
            System.out.println("Found module " + module);

            if (module != null) {
                module.processPayload(modulePacket.getPayload());
            }

        }
    }

    public void addAttribute(AttributeRef attributeRef, ModuleType type, DomintellAddress address, int ioIndex, Consumer<Value> consumer) {

        Module module = configuration.getModule(type, address, true);


        System.out.println("Add attribute on module " + module + ", configuration is " + configuration);
        // We might not have the configuration read in yet, do we want to create it
        // yes, build the configuration as we go
        // in first step, don't even read the app info
        // maybe second step see if can read appinfo as separate configuration and compare

        module.addConsumer(ioIndex, consumer);
    }

    public void updateValue(ModuleType type, DomintellAddress address, int ioIndex, Value value) {
        //BooleanValueImpl
        Module module = configuration.getModule(type, address);
        if (module != null) {
            ModulePacket message = module.messageForUpdateValue(ioIndex, value);
            if (message != null) {
                messageProcessor.sendMessage(message);
            }
        }
    }

}
