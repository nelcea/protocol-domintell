package be.nelcea.agent.protocol.domintell.model;

import be.nelcea.agent.protocol.domintell.DomintellAddress;
import be.nelcea.agent.protocol.domintell.ModuleType;
import org.openremote.model.util.Pair;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.Map;

/**
 * Java class description...
 * <p>
 * Date : 2019-03-03
 *
 * @author ebariaux
 */
public class Configuration {

    private Map<Pair<ModuleType, DomintellAddress>, Module> modules;

    public Configuration() {
        this.modules = new HashMap<>();
    }

    public void addModule(Module module) {
        modules.put(new Pair<>(module.getType(), module.getAddress()), module);
    }

    public Module getModule(ModuleType type, DomintellAddress address) {
        return getModule(type, address, false);
    }

    public Module getModule(ModuleType type, DomintellAddress address, boolean createIfMissing) {
        System.out.println("getModule(" + type + ", " + address + ")");
        System.out.println("Modules " + modules);
        System.out.println("Pair " + new Pair(type, address));
        Module module = modules.get(new Pair(type, address));
        if (module == null && createIfMissing) {
            System.out.println("Module not found but asked to create it");
            Class<? extends Module> moduleClass;

            try {
                // Look-up class that matches module type
                moduleClass = ModuleClassRegistry.getModuleClass(type);

                if (moduleClass == null) {
                    // We don't support it, skip to next module
                    return null;
                }
            } catch (IllegalArgumentException e) {
                e.printStackTrace();
                // Warn so we can improve protocol by supporting new modules
                // and skip to next module
                return null;
            }
            System.out.println("Module class " + moduleClass);

            try {
                Constructor<? extends Module> constructor = moduleClass.getConstructor(ModuleType.class, DomintellAddress.class);
                module = constructor.newInstance(type, address);
            } catch (SecurityException | NoSuchMethodException | IllegalArgumentException | InstantiationException | IllegalAccessException | InvocationTargetException e) {
                e.printStackTrace();
                return null;
            }
            System.out.println("Module created");
            addModule(module);
        }
        return module;
    }

}
