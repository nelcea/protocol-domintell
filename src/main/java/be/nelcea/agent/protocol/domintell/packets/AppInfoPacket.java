package be.nelcea.agent.protocol.domintell.packets;

import be.nelcea.agent.protocol.domintell.DomintellAddress;
import be.nelcea.agent.protocol.domintell.model.Configuration;
import be.nelcea.agent.protocol.domintell.InvalidDomintellAddressException;
import be.nelcea.agent.protocol.domintell.ModuleType;
import be.nelcea.agent.protocol.domintell.model.Module;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.StringReader;

/**
 * Java class description...
 * <p>
 * Date : 2019-03-03
 *
 * @author ebariaux
 */
public class AppInfoPacket extends MessagePacket {

    public AppInfoPacket(String message) {
        super(message);
    }

    public Configuration getConfiguration() {
        Configuration configuration = new Configuration();

        // Parse all lines until the END APPINFO one
        try (BufferedReader br = new BufferedReader(new StringReader(message))) {
            String line;
            while ((line = br.readLine()) != null) {
                if (line.startsWith("APPINFO")) {
//                WebSocket Client received message: APPINFO (PROG M 30.9 06/02/17 09h19 Rev=8 CP=UTF8) => Install A :
                } else if (line.startsWith("END APPINFO")) {
                    break;
                } else {
                    // Parse the module definition

                    // TODO: this is a partly copy from ModulePacket, should refactor
                    String typeText = line.substring(0, 3);
                    ModuleType type = ModuleType.valueOf(typeText);

                    String addressText = line.substring(3, 9);
                    DomintellAddress address;
                    try {
                        address = new DomintellAddress("0x" + addressText);
                    } catch (InvalidDomintellAddressException e) {
                        e.printStackTrace();
                        continue;
                    }

                    Module module = configuration.getModule(type, address, true);
                    if (module != null) {
                        module.parseConfiguration(line.substring(9));
                    }
                }


                /*
ET2    35[VERS=0x19]MOD DETH02[House||]
NT1    25[VERS=0x04]Module DNET01[House||]
IS8  1FAB-1Chambre HG[House||][PUSH=LONG]

                 */
            }

        } catch (IOException e) {

        }

        return configuration;
    }


    /*
     */
}
