package be.nelcea.agent.protocol.domintell.packets;

import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Java class description...
 * <p>
 * Date : 2019-03-02
 *
 * @author ebariaux
 */
public class InfoPacket extends DomintellPacket {

    public enum InfoType {
        WAITING_LOGIN,
        SESSION_OPENED,
        SESSION_TIMEOUT,
        HELLO_WORLD
    }

    static Map<String, InfoType> messageToEnum = Stream.of(new Object[][] {
            { "Waiting for LOGINPSW", InfoType.WAITING_LOGIN },
            { "Session opened", InfoType.SESSION_OPENED },
            { "Session timeout", InfoType.SESSION_TIMEOUT },
            { "World", InfoType.HELLO_WORLD}
    }).collect(Collectors.toMap(data -> (String)data[0], data -> (InfoType)data[1]));

    private InfoType type;

    public InfoPacket(String message) {
        type = messageToEnum.get(message);
    }

    public InfoType getType() {
        return type;
    }
}
