#TODO
- Request initial state of attribute values
- Handle deconnection cleanly
- Unlink attribute
- Loggers
- Documentation

#Questions

Is it worth keeping an in-memory representation of the whole Domintell network ?  
Could be created with APPINFO, but with no status info.  
This allows to validate if it's OK to add an attribute as we now if it exists in the installation. Might also be useful for autodiscovery.  
Can then actively request the state on that model to update our context.  

But is this some form of duplication of our context ?
